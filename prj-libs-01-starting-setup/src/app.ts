// Code goes here!

import axios from "axios";


//declare const google: any;
const form = document.querySelector('form')! as HTMLFormElement;
const addressInput = document.getElementById('address')! as HTMLInputElement;
const API_KEY = "AIzaSyA4XJHjrkz3Oo_KD6wmCnwOJrPF5eVDaIA";

type GoogleGeocodingResponse = {
    results: { geometry: { location: { lat: number, lng: number } } }[]
    status: 'OK' | 'ZERO_RESULTS'
}
function searchAddressHandler(event: Event) {
    event.preventDefault();
    const enteredAddress = addressInput.value;

    //send this to google's API 
    // Google Map API Key
    axios.get<GoogleGeocodingResponse>(`https://maps.googleapis.com/maps/api/geocode/json?address=${encodeURI(enteredAddress)}&key=${API_KEY}`).then(
        response => {
            if (response.data.status !== 'OK') {
                throw new Error('Could not fetch location!');
            }
            const coordinates = response.data.results[0].geometry.location;

            const map = new google.maps.Map(document.getElementById('map') as HTMLElement, {
                center: coordinates,
                zoom: 8
            });
            new google.maps.Marker({ position: coordinates, map: map });
        }
    ).catch(
        err => {
            alert(err.message);
            console.log(err)
        }
    );

}

form.addEventListener('submit', searchAddressHandler);