import experss, { Request, Response, NextFunction } from "express";
import todoRoutes from './routes/todos';
import json from "body-parser";
const app = experss();

app.use(json());
app.use('/todos', todoRoutes);

app.use((err: Error, req: Request, res: Response, next: NextFunction) => {
    res.status(500).send(err.message);
});

app.listen(4000);

//console.log("Something ......")

// npm init 
// tsc --init 

//npm install --save express body-parser

//restart node server if file change
//npm install --save-dev nodemon

//npm install --save-dev @types/node
// npm install --save-dev @types/express   