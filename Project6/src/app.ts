function Logger (logString: string ){
    console.log("Logger Factory")
    return function(constructor: any){
        console.log(logString)
        console.log(constructor)
    }
  
}


function WithTemplate(template: string , hookId : string){
    console.log("Teamplate Factory")

/*

    <T extends { new (...args: any[]) : {name: string}}>

    T <- generic

    new (...args : any []) : {name : string}<- create constructor and return obj with name properity
    
    new ():{} <- represent constructor function 

    return class extends orginalConstructor <- return the class which  extends the orginal obj 
    <-It can redefind the class for the class decoration
*/



    return function <T extends { new (...args: any[]) :{name : string}}>( orginalConstructor : T){
        /*console.log("Rendering Template ... ")
        const hookEl =document.getElementById(hookId)
        const p = new constructor()
        if (hookEl) {
            hookEl.innerHTML = template
            hookEl.querySelector('h1')!.textContent = p.name
        } */
        return class extends orginalConstructor{
            //unused param ( _ )
            constructor( ..._: any[]){
                super()
                // it will run when obj init 
                console.log("Rendering Template ... ")
                const hookEl =document.getElementById(hookId)
                if (hookEl) {
                    hookEl.innerHTML = template
                    hookEl.querySelector('h1')!.textContent = this.name
                } 
            }
        }
    }
}

//@Logger('LOGGING - PERSON')
@WithTemplate('<h1>My Person Object</h1>' ,'app')
class Person{
    name = 'Max'
    constructor(){
        console.log('Creating person object')
    }

     otherFunction(){
        console.log("Other function")
    }
}

const per = new Person()

function Log(target : any , propertyName: string | symbol){
    console.log("Property decoration")
    console.log(target , propertyName)
}

function Log2( target : any , name: string |symbol , descriptor :PropertyDescriptor){
    console.log("Accessor decorator!")
    console.log(target)
    console.log(name)
    console.log(descriptor)
}

function Log3( target : any , name: string |symbol , descriptor :PropertyDescriptor){
    console.log("function decorator!")
    console.log(target)
    console.log(name)
    console.log(descriptor)
}

function  Log4( target: any , name: string |symbol , position : number){
    console.log("paramter decorator!")
    console.log(target)
    console.log(name)
    console.log(position)
}



class Product {
    @Log
    title: string
    private _price : number

    @Log2
    set price(val : number){
        if(val > 0 ){
            this._price = val
        }else {
            throw new Error("Invalid price - should be positive!")
        }
    }

    constructor(t : string  , p : number){
        this.title = t
        this._price = p
    }

    @Log3
    getPriceWithTax(@Log4 tax : number) : number{
        return this._price*( 1 + tax)
    }
}

let prodObject = new Product("First product" , 2020)

function Autobind(_: any , _2 :string , descriptor :PropertyDescriptor){
    const orginalMethod = descriptor.value
    const adjDescriptor : PropertyDescriptor = {
        configurable: true, // property descriptor can be changed later
        enumerable: false, // would not be listed as property name in object 
        get(){ // run before you call the function 
            const boundFn = orginalMethod.bind(this)// "this" refer to the object which call this descriptor  
            return boundFn
        }
    };
    return adjDescriptor

} 
 


class Printer {
    message = 'This works!'

    @Autobind
    showMessage(){
        console.log(this.message)
    }
}


const p = new Printer()

const button = document.querySelector("button")!;
button.addEventListener('click' ,p.showMessage)


/*
On normal js case , 'this' will refer to the button object itself in the function passed on event listener.
Therefore , the "this.message" would be undefined due to the fact that the button object would not have the message field .
*/



interface ValidatorConfig{
    [property : string]:{
        [validatableProp:string]:string []
    }
}
const resgisteredValidator : ValidatorConfig= {}

function Required(target : any , propName: string){
    resgisteredValidator[target.constructor.name] = {
        ...resgisteredValidator[target.constructor.name],
        [propName] : ['required']
    }
}

function Positive(target : any , propName : string) {
    resgisteredValidator[target.constructor.name] = {
        ...resgisteredValidator[target.constructor.name],
        [propName] : ['positive']
    }
}

function validate( obj : any): boolean{
    const obValidatorConfig = resgisteredValidator[obj.constructor.name]
    if (!obValidatorConfig){
        return true
    }

    // in <- get property name
    //  of <- get value 
    let isValued : boolean = true
    for(const prop in obValidatorConfig) {
        for (const validator of obValidatorConfig[prop]){
            switch(validator){
                case 'required':
                    // !! <- check empty value
                    isValued = isValued &&!! obj[prop]
                    break
                case 'positive':
                    isValued = isValued && obj[prop] >0
                    break
            }
        }
    }
    return isValued
}

class Course {
    title:string
    price: number
    
    constructor(t :string , p:number){
        this.title = t
        this.price = p
    }
    
}

const courseForm = document.querySelector('form')!
courseForm.addEventListener("submit" ,event =>{
    event.preventDefault()
    
    var courseEml = document.getElementById("name")! as HTMLInputElement
    var priceEml = document.getElementById("price")! as HTMLInputElement
    
    var name  = courseEml.value
    var price = +priceEml.value // converst text to int 
    
    var course = new Course(name , price)
    

    
    console.log(course)
})