//最基本 annotation @Logger_2
function Logger_2(contructor: Function) {
    console.log('Logging ...')
    console.log(contructor)
    //Person_2 constructor 
}


// annotation 工廠模式
function LoggerFactor(logString: string): (contructor: Function) => void {
    console.log('LOGGER FACTORY')
    return function (constructor: Function) {
        console.log('Logging ...')
        console.log(constructor)
        console.log(logString)
        // class Person {
        //      constructor(){
        //         this.name = 'Max';
        //         console.log('Creating person object......')
        //      }   
        // }
    }
}


function WithTemplate2(template: string, hookId: string): (contructor: Function) => void {
    console.log('TEMPLATE FACTORY')
    return function (constructor: any) {
        console.log('Rendering template')
        const hookEl = document.getElementById(hookId)
        const p = new constructor() // 先create 一個object, 再把 object data 同 UI 掛鉤
        if (hookEl) {
            hookEl.innerHTML = template
            hookEl.querySelector('h1')!.textContent = p.name
            //! <- asume the h1 always exist 
        }
    }
}
// Top down approach
@LoggerFactor('LOGGING - PERSON')
@WithTemplate2('<h1>My Person Object</h1>', 'app')
// If the decorator is factory approach , the actual order would be changed (from down to top )
class Person_2 {
    protected name = 'Max';

    constructor() {
        console.log('Creating person object .....')
    }
}

const pers = new Person_2()
// Decorator function only run once when the class be defined , not object creation
// 就算在工廠模式, 也是只會在 class defined 時運行

function Log_2(target: any, propertyName: string | Symbol) {
    // can not return any thing 
    console.log('Property decorator!')
    console.log(target, propertyName)
    // target:
    // { 
    //     constructor: class Product,
    //     getPriceWithTax: f getPriceWithTax(tax)
    //     set price : f price(val)
    // }只顯示目標 class 的 function , 不包括 屬性
    // propertyName : title
}

function Log2_2(target: any, name: string, descriptor: PropertyDescriptor) {
    console.log("Accessor decorator!")
    console.log(target) // same as Log_2
    console.log(name)   // getPriceWithTax
    console.log(descriptor)
    // descriptor:
    // {
    //     get: undefined,
    //     set: f price(val),
    //     configurable: true,
    //     enumerable: false, # as a property on object loop 
    /*
        const p = new Product_2()
        for (let prop in p) {
            console.log(prop)
        }
    
    */
    // }

}

function Log3_2(target: any, name: string | symbol, descriptor: PropertyDescriptor) {
    console.log("Method decorator!")
    console.log(target) // the same as Log_2
    console.log(name)   // price
    console.log(descriptor)
    // descriptor:
    // {
    //     value: f getPriceWithTax(tax),
    //     writeable: true,# can be changed 
    //     configurable: true,
    //     enumerable: false, # as a property on object loop 

    // }
    /*
        Writeable: can change the value
        Configurable: can change the property or delete it
    
    */
}

function Log4_2(target: any, name: string | Symbol, position: number) {
    // can not return any thing 
    console.log('Parameter decorator')
    console.log(target)
    console.log(name)
    console.log(position)// start from zero : position index for arg
}


class Product_2 {
    @Log_2
    title: string;
    private _price: number
    @Log2_2
    set price(val: number) {
        if (val > 0) {
            this._price = val
        } else {
            throw new Error('Invalid price - should be positive')
        }
    }

    constructor(t: string, p: number) {
        this.title = t
        this._price = p
    }

    @Log3_2
    getPriceWithTax(@Log4_2 tax: number): number {
        return this._price * (1 + tax)
    }

}

// const product_2 = new Product_2("A", 11)
// product_2.getPriceWithTax = function (tax: number): number {
//     return 1
// }

// all decorators only run on class defination 

// decorators return type

// class decorators -> return constructor

//取代 contructor
function WithTemplate2_2(template: string, hookId: string) {
    console.log('TEMPLATE FACTORY')
    return function <T extends { new(...args: any[]): { name: string } }>(originalConstructor: T) {


        //T extends {new(...args: any[]): { name: string }}
        // new(...args:any[]) <- constructor with any argument  
        // : {name: string} <- object with name property 

        // replace the original constructor function
        // run on object create , not class defined  
        return class extends originalConstructor {
            constructor(..._: any[]) {
                // _ <- ignore the arguments
                super() // run orginal contructor 
                console.log('Rendering template')
                const hookEl = document.getElementById(hookId)
                if (hookEl) {
                    hookEl.innerHTML = template
                    hookEl.querySelector('h1')!.textContent = this.name
                    //! <- asume the h1 always exist 
                }
            }
        }
    }
}

@LoggerFactor('LOGGING - PERSON')
@WithTemplate2_2('<h1>My Person Object</h1>', 'app')
// If the decorator is factory approach , the actual order would be changed (from down to top )
class Person2_2 {
    name = 'Max';

    constructor() {
        console.log('Creating person object .....')
    }
}

function Autobind2(target: any, methodName: string, descriptor: PropertyDescriptor): PropertyDescriptor {
    const orginalMethod = descriptor.value;
    const adjDescriptor: PropertyDescriptor = {
        configurable: true,
        enumerable: false,
        get() {
            // call before the caller function run 
            // this <- refer to caller object
            const boundFn = orginalMethod.bind(this)
            return boundFn
        },
    }
    return adjDescriptor
}

class Printer_2 {
    message = 'This works'

    @Autobind2
    showMessage() {
        console.log(this.message)
    }
}

const p_2 = new Printer_2()

const button_2 = document.querySelector('button')!
button.addEventListener('click', p_2.showMessage)
// this.message <- undefined
// this -> refer to button event
//  p_2.showMessage.bind(p) -> this refer to object

interface ValidaorConfig {
    [property: string]: {
        [validatableProp: string]: string[] // ['required' , 'positive']
    }
}

const registeredValidators: ValidaorConfig = {};

function Required2(target: any, propName: string) {
    registeredValidators[target.constructor.name] = {
        ...registeredValidators[target.constructor.name], // list all value , can handle the undefined case
        [propName]: [
            ...(registeredValidators[target.constructor.name]?.[propName] ?? [])
            /* ?
            if !!registeredValidators[target.constructor.name]
                return registeredValidators[target.constructor.name][propName]
            else
                return underfined
            
            To more that one level access like registeredValidators[target.constructor.name][propName] ,
            
            if registeredValidators[target.constructor.name] is undefined, the above value will get exception directly.
            
            registeredValidators[target.constructor.name]?.[propName]
    
            if registeredValidators[target.constructor.name] is undefined , the returned value will get undefined driecly.
    
            ??
                if not falsy , use the first value  -> () ?? if falsy , use that value-> ()
            
            */
            , 'required']
    }

}

function PositiveNumber2(target: any, propName: string) {
    registeredValidators[target.constructor.name] = {
        ...registeredValidators[target.constructor.name],
        [propName]: [
            ...(registeredValidators[target.constructor.name]?.[propName] ?? [])
            , 'positive']
    }
}

function validate(obj: any): boolean {
    const objValidatorConfig = registeredValidators[obj.constructor.name]
    if (!objValidatorConfig) {
        return true
    }
    let isValid = true
    for (const prop in objValidatorConfig) { // property for loop (show name only)
        for (const validator of objValidatorConfig[prop]) { // array for loop
            switch (validator) {
                case 'required':
                    isValid = isValid && !!obj[prop]; // check property provided.  !!obj.name also available.
                    break;
                case 'positive':
                    isValid = isValid && obj[prop] > 0;
                    break;
            }
        }
    }
    return isValid;
}

class Course_2 {
    @Required2
    title: string
    @PositiveNumber2
    price: number

    constructor(t: string, p: number) {
        this.title = t
        this.price = p
    }
}

const courseForm2 = document.querySelector('form')!;

courseForm2.addEventListener('submit', event => {
    event.preventDefault()
    const titleEl = document.getElementById('title') as HTMLInputElement
    const priceEl = document.getElementById('price') as HTMLInputElement

    const title = titleEl.value
    const price = +priceEl.value // ensure the value is number

    const createdCourse = new Course_2(title, price)
    if (!validate(createdCourse)) {
        alert('Invalid input, please try again')
        return
    }

    console.log(createdCourse)
})
