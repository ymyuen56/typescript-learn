/// <reference path="base-component.ts"/>

namespace App {

    // ProjectInfo Class
    export class ProjectInput extends Component<HTMLDivElement, HTMLFormElement> {
        renderContent(): void { }

        titleInputElement: HTMLInputElement;
        descriptionInputElement: HTMLInputElement;
        peopleInputElement: HTMLInputElement;

        constructor(private projectState: ProjectState) {
            super('project-input', 'app', true, 'user-input')
            this.titleInputElement = this.element.querySelector('#title') as HTMLInputElement;
            this.descriptionInputElement = this.element.querySelector('#description') as HTMLInputElement;
            this.peopleInputElement = this.element.querySelector('#people') as HTMLInputElement;
            this.configure();
            this.renderContent();
        }

        configure() {
            this.element.addEventListener('submit', this.submitHandler);
        }

        // on tsconfig -> "experimentalDecorators": true,        /* Enables experimental support for ES7 decorators. */
        @autobind
        private submitHandler(event: Event) {
            event.preventDefault();
            const userInput = this.gatherUserInput();
            if (Array.isArray(userInput)) { // for tuple response ,  the tuple actually is an array.
                // Therefore, we need to check if the userInput is an array or not.
                const [title, desc, people] = userInput;
                this.projectState.addProject(title, desc, people);
                this.clearInputs();
            }
        }

        private clearInputs() {
            this.titleInputElement.value = '';
            this.descriptionInputElement.value = '';
            this.peopleInputElement.value = '';
        }

        private gatherUserInput(): [string, string, number] | void {
            const enteredTitle = this.titleInputElement.value;
            const enteredDescription = this.descriptionInputElement.value;
            const enteredPeople = this.peopleInputElement.value;
            const validateList: validatable[] = [
                {
                    value: enteredTitle,
                    required: true
                },
                {
                    value: enteredDescription,
                    required: true,
                    minLength: 5
                },
                {
                    value: +enteredPeople,
                    required: true,
                    min: 1,
                    max: 5
                }
            ]

            const isValidList = validateList.map(validateItem => validate(validateItem));
            if (isValidList.some(isValid => isValid === false)) {
                alert('Invalid input, please try again!');
                return;
            }
            return [enteredTitle, enteredDescription, +enteredPeople]; // +enteredPeople: convert string to number
        }
    }
}