console.log("start")
const names: Array<string> = [];

// Promise 用法
const promise: Promise<string> = new Promise((resolve , reject) => {
    setTimeout(()=>{
        resolve("10") // 成功
    },2000)
    
    reject("100") //失敗
})

promise.then(data =>{
    console.log(data.split(""))
}).catch( err =>{
    console.log(err)
})

// object 合並
function merge<T extends Object,U extends Object>(objA:T , objB: U):T&U{
    return Object.assign(objA , objB)
}

const mergedObj =merge<{name:string ,hobbies:string[]}, {age:number}>({name:"Max" , hobbies:['sports']} ,{age:30})
console.log(mergedObj)


interface Lenghty {
    length : number
}
// [] Tuple
function countAndDescribe<T extends Lenghty>( elements:T):[T , string]{
    let descriptionText = "Got no values"
    if (descriptionText.length > 0){
        descriptionText = "Got "+elements.length+" value"
    }
    return [elements , descriptionText]
}


console.log(countAndDescribe<string>("Hello"))
console.log(countAndDescribe<Array<string>>([""]))


function extractAndConvert<T extends object , U extends keyof T>(obj: T ,key: U){
    return 'Value' + obj[key]
}

extractAndConvert({ name:"Max"} , "name")
// interface | 可能值
class DataStorage<T extends string | number | boolean > {
    
    private data: T[] = []

    addItem(item: T){
        this.data.push(item)
    }

    removeItem(item: T ) {
        if (this.data.indexOf(item) === -1 ){
            return
        }
        this.data.splice(this.data.indexOf(item), 1)
    }

    getItems(){
        return [...this.data]
    }
}

const textStorge = new DataStorage<string>()
textStorge.addItem("Max")
textStorge.addItem("Manu")
textStorge.removeItem('Max')
console.log(textStorge.getItems())

/*
    const objStorge = new DataStorge<Object>()
    objStore.addItem({ name : "Max" })
    objStore.addItem({ name : "Manu" })
    objStore.removeItem({ name : "Max" }) // remove the last obj for the splite function
    console.log.(objStore.getItems())

*/

// Partial <- 把 interface / type 的成員設為可選 ,但不能直用在 class 
/*
如要在class 上使用，請先造一個新的 type
class Person {
    name: string;
    age: number;
    address: string;
}

type PartialPerson = Partial<Person>;
*/

interface CourseGoal {
    title: string
    description: string
    completeUntil: Date
}


function ceateCourseGoal(
    title : string,
    description: string,
    date: Date

): CourseGoal {

    let courseGoal : Partial<CourseGoal> = {};
    courseGoal.title= title
    courseGoal.description = description
    courseGoal.completeUntil = date
    return courseGoal as CourseGoal
}

const data : Readonly<String[]> = ["Hello"]


//data.push(" ")
// console.log(data.pull()




