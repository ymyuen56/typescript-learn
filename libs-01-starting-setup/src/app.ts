// Code goes here!

/*
import _ from "lodash";

console.log(_.shuffle([1, 2, 3]));

declare var GLOBAL: string;

console.log(GLOBAL)
*/
import { Product } from "./product.model";
import { plainToClass } from "class-transformer";
import "reflect-metadata"
import { validate } from "class-validator"

const products = [
    { title: "A Carpet", price: 29.99 },
    { title: "A Book", price: 10.99 }
]

const newProd = new Product("", -29.99)
validate(newProd).then(errors => {
    if (errors.length > 0) {
        console.log("Validation failed")
        console.log(errors)
    } else {
        console.log(newProd.getInformation())
    }
})


/*const loadedProducts = products.map(prod => {
    return new Product(prod.title, prod.price)
})*/

const loadedProducts = plainToClass(Product, products)

for (const prod of loadedProducts) {
    console.log(prod.getInformation())
}


// install @types/lodash for typescript checking
// sometimes the library may have  dts file 
// npm install @types/lodash --save-dev


// install class validator to validate class properties
// install class-transformer to transform json to class 
