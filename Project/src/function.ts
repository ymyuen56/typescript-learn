function addNumber(n1: number , n2 : number):number{
    return n1 + n2
}

function printResultFun(num: number):void{
    console.log("Result"+num)
    //return
}
//If you are going to defined  undefined return type , the return 'statment' must be includied

console.log(printResultFun(addNumber(5,12)))
//let result : Function
let result:(a: number, b:number)=>number
result = addNumber
console.log(result(8,8))

function addAndHandler( num1: number , num2:number ,cd :(n1: number)=>void){
    const result = num1 + num2
    cd(result)
    //Even though the signature of the functione is avoid  , the callback function still be ableto return value
}
addAndHandler(10,7,(result)=>{
    console.log(result)
})