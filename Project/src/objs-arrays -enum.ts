enum Role {ADMIN = 5, READ_ONLY = '6' , AUTHOR = 6}

type people = {
    name :string
    age : number 
    hobbies: string []
    role: [number , string] // fixed length
    role2: Role 
}

const person : people={
    name :"Jason Yeung",
    age  : 30 ,
    hobbies: ['Sports','Cooking'],
    role:[1,"author"],
    role2:Role.ADMIN
}

person.hobbies.push("Computer Game")
//person.role.push("programmer") // Typescript would not be able to known this error
//person.role =[1, "author","programmer"] the array must follow the type

//let a : any // any type can be assigned to the variable a
//let a : any[] // at lest , it should be array


console.log(person)
for (const hobby of person.hobbies){
    console.log(hobby.toLocaleUpperCase())
}

if (person.role2 == Role.ADMIN){
    console.log("Admin Role")
}

