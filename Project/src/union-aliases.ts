type combineable = number | string;
type ConversionDescriptor = 'as-number'|'as-text'



function combine(input1 : combineable , input2: combineable , resultConversion:ConversionDescriptor){
    let result 
    if (typeof input1 === "number" && typeof input2 === "number" || resultConversion === 'as-number'){
        result = +input1 + +input2 //convert to number type
    }else {
        result = input1.toString() + input2.toString()
    }
    return result
}

const combinedAges = combine(30 , 26 , "as-number")
console.log(combinedAges)

const combineName = combine('Jason','Yeung' , "as-text")
console.log(combineName)

const combinedStringAges = combine('30','26', 'as-number')
console.log(combinedAges)