let userInput : unknown
let userName : string

userInput = 5
userInput = 'Max'


if( typeof userInput === 'string'){
    userName = userInput
}
// The different part of any type 
//For the  unknown type , the type checking is required to assign other type of variable
 
function generateError(message : string , code : number) : never{
    throw{message : message , code : code}
    //while(true){}
}

generateError("An error occurred !", 500)

//The never type is for the function to stop / cash the script 


