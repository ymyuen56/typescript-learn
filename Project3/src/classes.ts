
//abstract class : 有 abstract method / varable
abstract class Department {
    // private name : string
    public static fiscalYear = 2019
    protected emplyees: string[] = []

    // 自動定義為 class property
    constructor(protected readonly id: string, public name: string) { }
    // <- another way to declare the 
    //It is goint to ensure  the "this" inside the function only can be refrected to the department object
    //Or , the object must the same as department
    /*
    department = {
        name : "",
        descript: ()=>{
            console.log("Department :"+ this.name)
        }
    }
    */
    //abstract s :string;
    abstract descript(this: Department): void;

    addEmployee(emplyee: string) {
        this.emplyees.push(emplyee)
    }

    readEmployee() {
        console.log(this.emplyees.length)
        console.log(this.emplyees)
    }

    // return 有 name property 的 class
    static createEmployee(name: string): { name: string } {
        return { name: name }
    }
}

class ITDepartment extends Department {

    descript(): void {
        console.log("IT Department id :" + this.id)
    }

    constructor(id: string, private admins: string[]) {
        super(id, "IT")
    }

    getAdmins(): string[] {
        return this.admins
    }
    printAdmin() {
        console.log(this.admins.length)
        console.log(this.admins)
    }

    addAdmin(admin: string) {
        this.admins.push(admin)
    }
}



class AccountingDepartment extends Department {
    descript(): void {
        console.log("Account Department :" + this.id)
    }
    private lastReport: string
    private static instance: AccountingDepartment

    // 不容許直接 new 
    private constructor(id: string, private reports: string[]) {
        super(id, "Accounting")
        this.lastReport = reports[0]
    }

    static getInstance(): AccountingDepartment {
        if (AccountingDepartment.instance) {

        }
        this.instance = new AccountingDepartment("b1", [])
        return this.instance
    }

    get mostRecentReport(): string {
        if (this.lastReport) {
            return this.lastReport
        }
        throw new Error("No Report Found")
    }

    set mostRecentReport(value: string) {
        if (!value) {
            throw new Error("Please pass in a valid value")
        }
        this.addReport(value)
    }



    addEmployee(emplyee: string) {
        if (emplyee == "Jason") {
            return
        }
        this.emplyees.push(emplyee)
    }

    getReport(): string[] {
        return this.reports
    }

    printReport() {
        console.log(this.reports.length)
        console.log(this.reports)
    }

    addReport(report: string) {
        this.reports.push(report)
        this.lastReport = report
    }
}
/*
const accounting = new Department( "","Accounting")
//console.log(accounting)
accounting.descript()

//const accountingcopy = { name: "" ,descript: accounting.descript}
//accountingcopy.descript()
//This object only have the function coping from accounting object .
//The object structure is not the same as the accounting object.


accounting.addEmployee("Jason")
accounting.addEmployee("Yeung")

accounting.readEmployee()
*/

const employee1 = Department.createEmployee("Jason")
console.log(employee1, Department.fiscalYear) // static data 

const itDepartment = new ITDepartment("", [])
itDepartment.addAdmin("Jason")
itDepartment.addAdmin("Max")

itDepartment.printAdmin()

const accounting = AccountingDepartment.getInstance() // static constractor -> need getInstance() to create object
const accounting2 = AccountingDepartment.getInstance()
accounting.addReport("IT Accounting Report")
accounting.addReport("Admin Accounting Report")

console.log(accounting, accounting2)

accounting.printReport()
// accounting.mostRecentReport = ''
//empty string <- (value) == false
accounting.mostRecentReport = "Year end report"
console.log(accounting.mostRecentReport)



accounting.addEmployee("Jason")
accounting.addEmployee("Yeung")
accounting.readEmployee()


const itDepartment2: ITDepartment = new ITDepartment("", itDepartment.getAdmins())
const departmentArray: Department[] = []
departmentArray.push(itDepartment)
departmentArray.push(itDepartment2)
departmentArray.push(accounting)

// for loop 的兩種寫法
for (let department of departmentArray) {
    department.descript()
}

departmentArray.forEach(
    (department) => {
        department.descript()
    }
)

// maping 的寫法
const newdepartmentArray = departmentArray.map((department) => {
    department.addEmployee("123JasonYeung")
    return department
})
newdepartmentArray.forEach(
    (department) => {
        department.readEmployee()
    }
)