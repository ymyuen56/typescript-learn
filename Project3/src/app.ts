

interface AddFn {
    (a:number , b: number):number
}

interface name {
    readonly name? : string //name! : string
    outputName?: string
}

interface greetable extends name{
    age?: number 
    greeet(pharse: string):void
}

class Person implements greetable{
    
    constructor(public name? : string , public age? : number ){}
    greeet(pharse: string): void {
        if (this.name){
            console.log(pharse +" "+ this.name)
        }else{
            console.log("Hi")
        }
    }    
}

let user1:greetable

/*user1 = {
    name :'Max',
    age : 30,
    greeet(pharse : string){
        console.log(pharse+" "+ this.name)
    }
}*/
user1 = new Person("Jason",13)

console.log(user1.greeet("Hello"))

let user2 :greetable = new Person()

console.log(user2.greeet(""))


let addOper : AddFn 
addOper = (a: number , b : number) : number => {
    return a+b
}
addOper(1,2)
