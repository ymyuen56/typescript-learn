const username = "Jason Yeung"
// username ="Jason"


function add(number1: number, number2: number): number {
    {
        var s: number = 1
        let s1: number = 2 // let variable can't be allowed to call outside the {} but no including function
        console.log(s1)
    }
    var result = number1 + number2 + s //+s1
    return result
}

//console.log(s) // var or let function variable can't be called outside the function 


const add1 = function (number1: number, number2: number = 1): number {
    return number1 + number2
}
// the paramter  can have default value <- but the paramater having 
// default value must be the last one 

const add2 = (number1: number = 6, number2: number): number => {
    return number1 + number2
}

const add3 = (number1: number, number2: number): number => (number1 + number2)


const printOutput: (i: number | string) => void = output => console.log(output)
/*
 (i : number | string ) =>  void  <- function type
 output => console.log(output) <- function
 paramater => return value | function call <- only one statment  
 */
const button = document.querySelector('button')!


button.addEventListener("click", event => console.log(event))



printOutput(add1(1))
//printOutput(add2(2))
// default value would not support on the first value of function 

const hobbies = ["Sports", "Cooking"]
const activeHobbies = ["Hiking", ...hobbies]

activeHobbies.push(...hobbies)

type personObj = {
    name: string,
    age: number
}

const person: personObj = {
    name: "Max",
    age: 30
}

const anotherPerson: personObj = {
    ...person
}
/*
*  it will list out all the element on person object  and use it to 
*  create another new one object.
*/

const add4 = (...num: number[]) => {
    // ...num <-mostly ,it will be array 
    return num.reduce((result: number, num: number): number => {
        return result + num
    }, 0)
}

// array function  arrays.reduce( handler : (result : number , num : number)=>number , start : number )
// this function will go through all the element on loop and return one value

const add5 = (...num: [number, number, number]) => {
    return num[0] + num[1] + num[2] //tuple
}


printOutput(add4(1, 2, 3, 4, 5, 6, 7))
printOutput(add5(1, 2, 3))

const [hobby1, hobby2, ...remainingHobby] = hobbies
/*
 * it will assign the element on lood into the list of variable.
 * hobby1 = hobbies[0]
 * hobby2 = hobbies[1]
 * remainingHobby <- the remaining element on hobbies apart from the first and second one
 */
// it will  assign the value on 
console.log(hobbies, hobby1, hobby2)

const { name: userName, age } = person
/*
 * the name inside { } should be equal to the properity name on person object
 * the properity on person oject will be assigned to the value on object 
 * "name:userName" <- it allow the person.name properity be called by "Username"
 */
console.log(userName, age)