import { ProjectState } from "./state/project-state";
import { ProjectInput } from "./components/project-input";
import { ProjectList } from "./components/project-list";

const projectState = ProjectState.getInstance();
new ProjectInput(projectState);
new ProjectList('active', projectState);
new ProjectList('finished', projectState);

console.log("Hihi")


//npm install --save-dev webpack webpack-cli webpack-dev-server typescript ts-loader
// webpack : able to translate typescript to javascript , bundle the translated javascript files
// webpack-cli : run webpack command line
// webpack-dev-server :
//  start webpack in backend
//  monitoring file change
//  recomplie file when file change
// typescript: set up local version typescript (by project)
// ts-loader : loader for webpack to translate typescript to javascript

// config: tsconfig.json
//  1. set target to "es6" / "es5" , which depends on which version of javascript you want to use
//  2. set module to "es2015"/"es6"
//  3. set outDir to "./dist" (webpack output directry)
//  4. remove rootDir , webpack will control where the root directory are.
//create webpack.config.js 
// 1. remove all .js on ts file import 
//create webpack.config.js

//Plugin
// npm install --save-dev clean-webpack-plugin
// usage clean dist floder 