import Component from "./base-component";
import { autobind } from "../decorators/autobind";
import { Project, ProjectSatus } from "../models/project";
import { ProjectItem } from "./project-item";
import { ProjectState } from "../state/project-state";
import { DragTarget } from "../models/drag-drop";


// Each code only be excuted once even though multiable time import

// ProjectList Class
export class ProjectList extends Component<HTMLDivElement, HTMLElement> implements DragTarget {

    assignedProjects: Project[] = [];

    constructor(private type: 'active' | 'finished', private projectState: ProjectState) {
        super('project-list', 'app', false, `${type}-projects`)
        this.configure();
        this.renderContent();
    }


    @autobind
    dragOverHandler(event: DragEvent): void {
        if (event.dataTransfer && event.dataTransfer.types[0] === 'text/plain') {
            event.preventDefault();
            // If you not prevent the default event, the drop action would not be triggered.
            const listEl = this.element.querySelector('ul')!;
            listEl.classList.add('droppable');
        }
    }
    @autobind
    dropHandler(event: DragEvent): void {
        const priId = event.dataTransfer!.getData('text/plain');
        this.projectState.moveProject(priId, this.type === 'active' ? ProjectSatus.Active : ProjectSatus.Finished);
    }

    @autobind
    dragLeaveHandler(_: DragEvent): void {
        const listEl = this.element.querySelector('ul')!;
        listEl.classList.remove('droppable');
    }

    configure(): void {
        this.element.addEventListener('dragover', this.dragOverHandler);
        this.element.addEventListener('drop', this.dropHandler);
        this.element.addEventListener('dragleave', this.dragLeaveHandler);

        this.projectState.addListener({
            func: (projects) => {
                this.assignedProjects = projects.filter(prj => {
                    if (this.type === 'active') {
                        return prj.status === ProjectSatus.Active;
                    }
                    return prj.status === ProjectSatus.Finished;
                })
                this.renderProjects();
            }
        })
    }


    private renderProjects() {
        const listEl = document.getElementById(`${this.type}-projects-list`)! as HTMLUListElement;
        listEl.innerHTML = ""
        for (const prjItem of this.assignedProjects) {
            /* const listItem = document.createElement('li');
             listItem.textContent = prjItem.title;
             listEl.appendChild(listItem);*/
            new ProjectItem(this.element.querySelector('ul')!.id, prjItem);
        }
    }

    renderContent() {
        const listId = `${this.type}-projects-list`;
        this.element.querySelector('ul')!.id = listId;
        this.element.querySelector('header')!.querySelector('h2')!.textContent = this.type.toUpperCase() + ' PROJECTS';
    }
}
