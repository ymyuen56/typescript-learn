import { Project } from "../models/project";
import { ProjectSatus } from "../models/project";

type listenerFuc<T> = {
    func: (projects: T[]) => void;
}

class State<T> {
    protected listeners: listenerFuc<T>[] = [];

    addListener(listenerFn: listenerFuc<T>) {
        this.listeners.push(listenerFn);
    }
}

export class ProjectState extends State<Project> {
    private projects: Project[] = [];
    private static instance: ProjectState;

    private constructor() {
        super()
    }

    static getInstance(): ProjectState {
        if (this.instance) {
            return this.instance;
        }
        this.instance = new ProjectState();
        return this.instance;
    }

    addProject(title: string, description: string, numOfPeople: number) {
        this.projects.push(new Project(title, description, numOfPeople));
        this.updateListeners();
    }

    moveProject(projectId: string, newStatus: ProjectSatus) {
        const project = this.projects.find(prj => prj.id === projectId);
        if (project && project.status !== newStatus) {
            project.status = newStatus;
            this.updateListeners()
        }
    }

    private updateListeners() {
        for (const listener of this.listeners) {
            listener.func(this.projects.slice());
        }
    }
}

