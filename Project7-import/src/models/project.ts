
// Project Type
export enum ProjectSatus { Active, Finished }

export class Project {
    public id: string = '';
    constructor(
        public title: string,
        public description: string,
        public people: number,
        public status: ProjectSatus = ProjectSatus.Active
    ) {

        this.id = Math.random().toString();
    }
}
