type Admin = {
    name: string
    privileges: string[]
}

interface Admin2 {
    name: string
    privileges: string[]
}


type Employee = {
    name: string
    startDate: Date
}

interface Employee2 {
    name: string
    startData: Date
}
// interface 和 type 基本一樣


type ElevatedEmployee = Admin & Employee
// & operation can generate new type without repreated element
/*
  type ElevatedEmployee ={
    name : string
    privileges : string[]
    startData : Date 
  }
 */
type ElevatedEmployee2 = Admin2 & Employee2
//interface also can be treated as type in this case
/*
 you can't use interface for the type of new generated one
 For example :
    interface ElevatedEmployee2 = Admin2 & Employee2   
*/

const e1: ElevatedEmployee = {
    name: 'Max',
    privileges: ['create-server'],
    startDate: new Date()
}

type Combinable = string | number
// only can be string type or number type
type Numeric = number | boolean

type Combine = Combinable & Numeric
// the combine type are number type
/*
   (sting | number) & (number | boolean)
 */

//類似 java 的 overload , 一個 function 可以有不同的簽署
function add(a: number, b: number): number
function add(a: string, b: string): string
function add(a: Combinable, b: Combinable): Combinable {
    if (typeof a === 'string' || typeof b === 'string') {
        // check the variable but it only avaiable for 
        /*
        Type of the operand	Result
        Object	    "object"
        boolean	    "boolean"
        function	"function"
        number	    "number"
        string	    "string"
        undefined	"undefined"
        */
        return a.toString() + b.toString()
    }
    return a + b
}
const result = add("Jason", "Yeung")
result.split("")

/*
In case , the type of reture value can't be defined like 
type result = string | number 

The function overload feature can help the typescript  complier to
defined what the type of function return value is .
*/



type UnknownEmployee = Employee | Admin

function printEmployeeInformation(emp: UnknownEmployee): void {
    console.log("Name :" + emp.name)
    if ('privileges' in emp) { //檢查 emp 是否有 相同的成員名稱
        /*
         check whether the property is inside the object 
         'property_name' in obj
        */
        console.log("Privilage" + emp.privileges)
    }

    if ('startDate' in emp) {
        console.log('Start Date' + emp.startDate)
    }
}

printEmployeeInformation({ name: 'Manu', startDate: new Date() }) //唔需要 new Object. 只要

class Car {
    drive() {
        console.log("Driving ...")
    }
}

class Truck {
    drive() {
        console.log("Driving ...")
    }
    loadCargo(amount: number) {
        console.log("Loading cargo ....." + amount)
    }
}

type Vechicle = Car | Truck

const v1 = new Car()
const v2 = new Truck()

function useVehicle(vechicle: Vechicle) {
    vechicle.drive()
    if (vechicle instanceof Truck) { // 檢查是否為 Truck object
        /*
         check whether the object belong to same class
          object-name instanceof class name
        */
        vechicle.loadCargo(10)
    }
}

useVehicle(v1)
useVehicle(v2)

interface Bird {
    type: 'bird' //  it is going to force the "type" variable only can be 'bird'
    flyingSpeed: number
}

interface Hourse {
    type: "hourse"
    runningSpeed: number
}

type Animal = Bird | Hourse
/*
for the  |  case , the variable "type" will be the way 
to distinguish which class does the animal object belog to 'Bird' or 'Hourse'
*/


function moveAnimal(animal: Animal): number {
    let speed
    switch (animal.type) { // <- 使用相同的 field , 作為決定最終 type 的依據
        case "bird":
            speed = animal.flyingSpeed
            break
        case "hourse":
            speed = animal.runningSpeed
            break
    }
    if (speed === undefined) {
        throw new Error("number not defined")

    }
    return speed
}

try {
    let animal: Animal = {
        type: "hourse",
        runningSpeed: 10
    }
    console.log("animal moving speed ..." + moveAnimal(animal))
} catch (err) {
    console.log(err)
}



/*
 type casting solution
 */
//solution 1
//let userinput = document.getElementById('user-input')! as HTMLInputElement
/*
   !<- force "userinput" would not be null
   as <- convert to HTMLInputElement 
*/
//solution 2
//let userinput2 = <HTMLInputElement> document.getElementById('user-input')

//userinput.value = "Hello"

let userinput = document.getElementById('user-input')

if (userinput) {
    (userinput as HTMLInputElement).value = "Hello"
}

/*
 checking whether it can be found
 and do type casting before assigning the value
*/

interface ErrorContainer {
    [prop: string]: string
}
/*
 It is going to defined the interface with unknown property.
 But the type of the property name and property value must follow definition
  [prop : string] : string
  the property name must be string .
  the property value also need to be string.

In addition , all the property should follow the same row even though the predefined one


The syntax [prop: string]: string specifies that the object can have any number of properties with string keys and string values.
const myError: ErrorContainer = {
    errorCode: "404",
    errorMessage: "Page not found"
}
*/


const fetchedUserData = {
    id: "u1",
    name: "Max",
    job: {
        title: "CEO",
        descrption: "My own company"
    }
}

//console.log(fetchedUserData?.job?.title)
/*
    Optional Chaining
    it allow the commant to check whether the object have the property.

    Actually , it is equal to the 
    if ('job' in fetchedUserData){
        jobObject = fetchedUserData.job
        if ('title' in jobObject) {
            console.log(jobObject.title)
        }
    }
*/


const userInput = undefined

// normal  practice 

const storeData = userInput || "DEFAULT"
/*
使用 default value 的方法
 If the userInput is equal to falsy value , the storeData would be
 "DEFAULT"

Falsy Value:
false:	The keyword false
0:	The number zero
0n:	BigInt, when used as a boolean, follows the same rule as a Number. 0n is falsy.
"", '', ``
:This is an empty string (the length of the string is zero). Strings in JavaScript can be defined with double quotes "", single quotes '', or Template literals ``.
null:	null - the absence of any value
undefined:	undefined - the primitive value
NaN:	NaN - not a number
*/


//const storeData2 = userInput ??  "DEFAULT"
/*
?? 的作用相等於似下造法(檢查 null 和 undefined)
if (userInpur !== null && userInput !== undefined) {
    storeData2 =  userInput
}else {
    storeData2 =  "DEFAULT"
}
*/